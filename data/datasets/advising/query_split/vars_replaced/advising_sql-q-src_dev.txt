What classes next semester are available as ULCS ?
Any ULCS classes available next term ?
Any available ULCS next semester ?
Are any classes available next semester for PreMajor fulfillment ?
Are there any classes available next semester as MDE ?
For PreMajor , what classes can I take next semester ?
In the next semester which classes are available as ULCS ?
Name the classes next semester which are available as MDE ?
Next semester , what ULCS courses are available ?
Next semester , what classes are available as PreMajor ?
Next semester , what classes can I take that are Core ?
Next semester which classes are available as PreMajor ?
What classes available as MDE are there for next semester ?
Which classes are available as Other from the classes next semester ?
Which classes are available as PreMajor next semester ?
Which of next semester 's classes can be taken as MDE ?
What classes do n't have lab sessions ?
Are there any classes that do n't have lab sessions ?
As far as labs go , are there any classes without them ?
Do any classes not have lab sessions ?
For classes , which ones do n't have labs ?
If I do n't want to have a lab session , which classes should I take ?
Is there a list of classes that do n't have lab sessions ?
List classes without lab sessions .
Of these classes , which do n't have lab sessions ?
What classes do not have sessions in the lab ?
What classes have no lab sessions ?
What courses are not in the lab ?
Which classes do n't have any labs ?
Which classes do not have lab components ?
Which classes do not require a lab session ?
Which of the classes do n't have lab sessions ?
Tell me more about UKR 450 .
160 THEORY is about ?
Can you tell me more about LHSP 229 ?
Could you tell me about NERS 583 ?
MECHENG 499 is ?
BIOMATLS 990 is what ?
Explain what NATIVEAM 405 contains .
Tell me about ANATOMY 541 .
What 's RCARTS 389 about ?
What 's there in EDUC 401 ?
What does ACABS 498 cover ?
What does STRATEGY 564 entail ?
What is MILSCI 101 about ?
What is the BIOMEDE 556 course about ?
What is the class CICS 301 about ?
What is the topic of MEDEDUC 505 ?
What kinds of things will I learn in HEBREW 203 ?
What material does SW 642 go over ?
What materials are covered in ACABS 860 ?
What subjects does COMM 490 cover ?
Are 300 -level classes offered in Spring term or Summer term ?
Are there any 100 -level courses in the Spring or Summer term ?
Can 500 -level classes be taken in the Spring or Summer term ?
In the Spring or Summer term are there 500 -level classes available to take ?
Is it possible to take 100 -level classes in Spring or Summer ?
Are 400 -level courses offered Spring or Summer terms ?
In the Spring or Summer are 500 -level classes available ?
During the Spring and Summer terms are 500 -level classes offered ?
Are there any 500 -level classes offered in the Spring or Summer term ?
Can a student take 300 -level classes Spring or Summer terms ?
During Spring or Summer term , are there 400 -level classes offered ?
Are there 300 -level courses in Spring or Summer term ?
What 500 -level classes are offered in the Spring or Summer terms ?
Are 500 -level classes offered in Spring or Summer ?
Are 200 -level classes available in Spring or Summer term ?
The 200 -level classes , are they available in Spring or Summer terms ?
Do the Spring terms have the 500 -level classes , or Summer terms ?
What other courses does Prof. Ayaka Sogabe teach besides 424 ?
Apart from 551 , what courses does Prof. James Crowfoot teach ?
Besides 515 , what other courses does Prof. Anne Greenberg teach ?
Besides 492 what classes does Prof. Barry Fishman teach ?
Can you list the courses other than 627 that Prof. Brendan Goff teaches ?
Does Prof. Thomas Petersen only teach 101 ?
Does Prof. William Iii teach other courses besides 646 ?
Other than 957 , what courses does Prof. Pedro Monaville teach ?
Prof. Catherine Sanok teaches what other courses besides 468 ?
What courses , besides 524 , does Prof. Saumuy Puchala teach ?
What courses does Prof. Paul Shearer teach , excluding 183 ?
What courses does Prof. Rachel Croskery-roberts teach besides 329 ?
What other courses , besides 129 , does Prof. Ruth Caston teach ?
Which courses besides 218 are available with Prof. Jorge Martinez ?
Which courses does Prof. Shelly Alilunas teach in addition to 672 ?
When will Developmental Disturbances of Childhood be offered next ?
At what point will Practicum Kines be offered ?
The Teaching Percussion Instruments course will be offered when ?
What is the upcoming availability for Multidisciplinary Engineering Design ?
When is Construction Management Information System going to be offered next ?
When is the next offering of Computational Economics ?
When is the next time the Pharmacotherapeutics will be offered ?
When will I have the opportunity to take Business Systems Consulting again ?
When will The Academic Paradox be offered again ?
When will Early Renaiss Music next be offered ?
When will the class `` Statistical Models and Numerical Human Genetics '' be offered ?
When will the next `` Utilization of Nursing Advanced Practice '' class take place ?
When will the next Worksite Wellness class be offered ?
What time will 516 and 795 be over as I need to leave by 5:00 P.M. each day to get to work on time .
I need to get going to my job at 5:00 P.M. in order to get there on time ; when will 400 and 327 end for the day ?
What time will 210 and 139 be over , given that I need to get to work on time each day by leaving by 5:00 P.M. ?
Since I need to leave by 5:00 P.M. each day to get to work on time , what time will 153 and 149 be over ?
I need to know when 145 and 630 will be as I need to get going by 5:00 P.M. if I want to be to work on time .
In order to be on work on time I need to leave by 5:00 P.M. , can you tell me when 596 and 851 will be over ?
When will 794 and 345 be over if I need to get to work on time before 5:00 P.M. .
What time will 595 and 853 be over as I need to leave by 5:00 P.M. each day to get to work on time ?
I have to leave by 5:00 P.M. in order to arrive at work on time , what time will 464 and 384 finish up ?
I need to leave by 5:00 P.M. every day so what time will 205 and 344 end ?
Since I need to leave by 5:00 P.M. to be on time for work every day , can I ask what time 362 and 779 are over ?
If I need to leave at 5:00 P.M. , can you tell me when 877 and 729 end ?
Since I have to leave by 5:00 P.M. to get to work on time every day , what time will 416 and 336 be over ?
Show me courses that involve parts of Senior Comparative Literature and Advanced Historical Research .
Are there courses that involve parts of Applied Regression Analysis and Interdisciplinary the Middle Ages .
Can you show me which courses involve parts of Modern Dance and Restoration Ecology .
Display courses that involve any parts of Elementary Modern Greek and Distributed Systems .
I want to see any courses that have something to do with Molecular Biology of Pain and Sensation and Tactical Approach to Invasion Games .
I want to see only those courses which involves part of Senior Sociology and Technical Communication for Electrical and Computer Science .
Name the courses that include parts of Space-time Symmetries and the Representations and The Graphic Narrative .
Of all the courses show me the ones that involve parts of Information Industry and Sociocultural Planning and Architecture .
Show me any courses that cover parts of Contemporary Social Southeast Asia and Utilization of Nursing Advanced Practice .
Show me classes that involve parts of Plant Physiological Ecology and Collat Conseq of Crim Conv .
Show me courses that include any parts of Digital Communication Signals and Systems and Devising Theatre .
Show me courses that incorporate Preliminary Exam and Viscous Flow .
Show me courses which involve parts of Marine Structures and Writing Poetry .
What are the courses that involve parts of Conversation through Film and Popular Music and Lighting Design ?
What classes involve Special Naval Architecture and Marine Engineering and Orthodontics Practice Management and Transitions ?
Which courses involve parts of Integ Microsys Lab and Health Care Markets and Public Policies , can you show me ?
What are the easiest courses ?
The easiest courses are ?
What are the easiest classes ?
What are the simplest courses ?
Which classes are easiest ?
Which classes are the least difficult ?
Which courses are easiest ?
Which courses are the least difficult ?
Which courses are least difficult ?
Which courses are the least difficult ?
What is the name of PHRMACOL 616 ?
MILSCI 302 name is ?
What 's BIOLCHEM 712 ?
What class is BIOLCHEM 576 ?
What course name does LACS 483 have ?
What is MATSCIE 489 ?
What is CJS 592 really called ?
What is the actual name of ELI 530 ?
What is the course CSP 100 ?
What is the course name for CCS 597 ?
What is the course name of BIT 399 ?
What is the full name of course MCDB 501 ?
What is the name of the RCLANG 290 class ?
What is the title of MATH 216 ?
Which programs require LATIN 195 ?
In what programs is it necessary to take EEB 485 ?
In which programs is RESTORA 744 a requirement ?
YIDDISH 101 is a prerequisite for which programs ?
ORGSTUDY 202 is a requirement in which programs ?
BIOLCHEM 416 is compulsory in which programs ?
MO 617 is required by which programs ?
PORTUG 301 is required for which programs ?
What are the programs for which ORGSTUDY 299 is a requirement ?
What are the programs that require EARTH 423 ?
Which degrees require STRATEGY 411 ?
Which programs have ASTRO 160 as a prerequisite ?
Which programs require you to take SM 444 ?
What 's the difference between SI 523 and SI 546 ?
Between ARABIC 202 and ARABIC 401 , how are they different ?
Can you tell me what 's different between THTREMUS 442 and THTREMUS 360 ?
Differentiate between DANCE 213 and DANCE 335 .
How are AOSS 574 and AOSS 321 different ?
How does BIOMEDE 590 differ from BIOMEDE 990 ?
How is DHYGRACK 688 different from DHYGRACK 683 ?
Name the difference between LAW 441 and LAW 946 .
Tell me the difference between courses PSYCH 877 and PSYCH 533 .
Tell me the differences between ARABIC 600 and ARABIC 101 .
What 's different between the ANAT 504 class and the ANAT 499 class ?
What 's the difference between STDABRD 475 and STDABRD 456 ?
What 's the difference between courses RCNSCI 104 and RCNSCI 232 ?
What are the major differences between ARCH 466 and ARCH 591 ?
What is the difference between the courses PHYSED 414 and PHYSED 142 ?
Which courses does Prof. Stephen Cooper teach ?
Are there any courses taught by Prof. Nicholas Gaudio ?
List the courses Prof. Yen Lin teaches .
Name the courses taught by Prof. Bradley Moore .
Prof. Sebouh Aslanian teaches what classes ?
Prof. Emily Harrington teaches what courses ?
Prof. Wilhelm Piskorowski teaches which courses ?
The names of Prof. Annette Masson 's courses ?
What are all the courses Prof. Marta Kondratyuk teaches ?
What are the names of the courses Prof. Trevor Legassick teaches ?
What classes will Prof. Mary Simoni be teaching ?
What courses are taught by Prof. Sarah Miller ?
What courses is Prof. Earl Werner the teacher of ?
What does Prof. Kristina Daugirdas teach ?
Which courses have Prof. Danielle Czarnecki as the teacher ?
Which courses is Prof. Matthew Ronfeldt teaching ?
How many lectures does REES 410 have each week ?
AOSS 411 has how many lectures a week ?
SPACE 101 has how many lectures every week ?
NERS 421 has how many lectures per week ?
Each week , how many lectures does SURVMETH 742 have ?
Every week , how many lectures are offered by MEDADM 515 ?
How many MILSCI 202 lectures are there each week ?
How many lectures are there for REEES 801 every week ?
How many lectures each week does CDB 598 have ?
How many times a week does AERO 202 meet ?
What 's the number of weekly lectures for AAPTIS 289 ?
What 's the total number of weekly lectures for MACROMOL 412 ?
What are the PreMajor classes ?
Can you tell me the PreMajor classes ?
Core classes are ?
Name the ULCS classes .
Name the MDE classes .
Of all the classes , which are the ULCS ones ?
Regarding Other classes , which are they ?
The PreMajor classes are ?
What are the Other classes ?
What are the names of the MDE classes ?
What Other classes are there ?
Which all are the MDE classes ?
Which are the Other classes ?
Which classes are ULCS classes ?
Which classes are the MDE ones ?
Which ones are the PreMajor classes ?
Can you tell me all of the PreMajor classes ?
All of the PreMajor classes are ?
Could you please name all of the PreMajor classes for me ?
PreMajor classes are ?
May I know all of the MDE classes from you ?
Name all MDE classes for me .
Name all the ULCS classes for me .
Name for me all PreMajor classes .
Name for me all the ULCS classes .
Please tell me , what are all the ULCS classes ?
What are all of the PreMajor classes ?
What are all the courses for Other ?
What are the all of the PreMajor classes ?
Would you mind informing me what all of the ULCS classes are ?
Can I see my transcript ?
Am I able to view my transcript ?
Bring up my transcript .
Can I view my transcript ?
Can you show me my transcript ?
I need to see my transcript .
I want to see my transcripts .
I would like to see my transcript .
Is it possible to see my transcript ?
Is my transcript something that 's available for me to see ?
May I look at my transcript ?
May I please see my transcript ?
May I see my transcript ?
May I view my transcript ?
My transcript , can I see it ?
Please let me see my transcript .
Show me my transcript .
What are the course prerequisites for ANTHRCUL 272 ?
Are there any courses that are prerequisites for MOVESCI 489 ?
Can you tell me what the course prerequisites are for BIOMEDE 523 ?
Does EEB 453 have any prereqs ?
Does MODGREEK 201 have any prerequisite courses ?
RUSSIAN 550 has which courses as prerequisites ?
For BIOPHYS 995 , what are the prerequisites ?
For MKT 601 what are the course prerequisites ?
For TURKISH 102 which are the prerequisites courses ?
Name the courses required for PORTUG 232 ?
Name the courses that are mandatory for SI 546 ?
Off all the courses which ones are prerequisites for ANTHRCUL 338 ?
Required courses for BE 619 are ?
The course prerequisites for PHYSED 425 are ?
The prerequisite courses for PERIODON 793 are ?
What are MKT 322 's course prerequisite requirements ?
What are the course requirements for THEORY 721 ?
What classes need to be taken before ENS 350 ?
What course prerequisites does GEOSCI 452 have ?
What exactly are the prerequisites for the course ORTHO 767 ?
What prerequisites are required for ENSCEN 451 ?
Which days of the week is BIT 630 taught ?
About BIOLCHEM 398 , do you know what days it 's taught on exactly ?
Can you tell me the days of the week that TCHNCLCM 380 is taught ?
PHYSED 415 is offered on which days ?
MEMS 382 is taught in which days of the week ?
CLARCH 856 is taught on what days of the week ?
CHE 489 is taught on which days of the week ?
ANAT 403 is taught when in the week ?
In regards to PHYSED 265 , what days during the week is the class held ?
Name the days of the week HUMGEN 553 is taught .
Name the days when CONDUCT 415 is taught .
On what days of the week are the SWC 300 classes ?
What days is ROMLING 450 going on ?
What days of the week does PHIL 157 meet ?
When are the COMPLIT 374 classes in the week .
When does the class meet for RCHUMS 218 ?
Which days is BCS 232 taught ?
Which days is ECON 621 taught in a week ?
Does ANATOMY 571 count for ULCS ?
Can ORALPATH 696 be used for ULCS ?
Can I take MUSICOL 478 for ULCS ?
Do I count AAS 458 for ULCS ?
Does ANAT 403 work for ULCS ?
SI 629 counts for ULCS ?
HJCS 405 counts for a ULCS ?
For ULCS , does REEES 695 count ?
For ULCS does COMP 425 count ?
For a ULCS does MEDEDUC 499 count ?
Is PUBPOL 571 accepted as ULCS ?
Is LHSP 229 applicable for ULCS ?
Is EHS 869 counted towards ULCS ?
Will CMBIOL 681 count towards the ULCS ?
Would RCIDIV 351 count towards ULCS ?
Would MODGREEK 305 fit the ULCS requirements ?
Would EEB 412 qualify for ULCS ?
Are there any 200 -level classes in Spring or Summer term ?
100 -level classes , are they offered in the Spring or Summer term ?
Any 400 -level classes held in Spring or Summer term ?
Are 100 -level classes available in either the Spring or Summer terms ?
Are 500 -level classes available in Spring or Summer term ?
Are any 100 -level classes being offered in the Spring or Summer term ?
Are there any Spring or Summer classes available for the 100 -level ?
Can 200 -level classes be taken in Spring or Summer ?
Can the 300 -level classes be taken in the Spring or Summer term ?
Do any 100 -level classes occur in the Spring or Summer term ?
Do you know if 500 -level classes are being held in Spring or Summer term ?
Does the Spring or Summer term offer any 100 -level classes ?
During the Spring or Summer terms , are any 400 -level classes being offered ?
For the Spring or Summer term , are there any 500 -level classes ?
In Spring or Summer term are there any 100 -level classes ?
In the Spring or Summer term are there 200 -level classes available to take ?
In the Spring or Summer term are there any 400 -level classes ?
What 300 -level classes do you have available for the Spring or Summer term ?
What 200 -level courses are available in Spring or Summer terms ?
Which 300 -level classes are being taught in Spring and Summer term ?
Which 100 -level classes are there for Spring or Summer terms ?
How many sections of PORTUG 235 are there ?
BIT 311 contains how many sections ?
EPID 788 has how many sections ?
For ITALIAN 425 , how many section are there ?
For PHIL 460 , how many sections are there ?
For INSTHUM 611 , what is the number of sections available ?
How many sections does IOE 801 have ?
How many sections does MILSCI 499 include ?
How many sections make up DENT 533 ?
Of ASTRO 389 , how many sections are there ?
What is the quantity of MILSCI 401 sections ?
What is total number of ARABAM 405 sections ?
What number of sections of AERO 311 are there ?
What 300 -level classes are offered next semester ?
Are 300 -level classes offered next semester ?
Can you list the 300 -level courses for next semester ?
For our next semester , will 300 -level classes be offered ?
I need a list of the 300 -level classes offered next semester .
In the next semester , which 300 -level courses will be available ?
In the next semester if 300 -level classes are offered what kind of classes can be taken ?
Is there a list for next semester of 300 -level courses being offered ?
Next semester , what 300 -level classes are offered ?
Next semester , what 300 -level classes are there ?
Next semester , what 300 -level classes will be offered ?
Next semester , what are the 300 -level classes ?
Next semester , what classes are offered at 300 -level ?
Next semester , which 300 -level classes are available ?
What 300 -level classes can I anticipate next semester ?
What are next semester 's 300 -level classes ?
What classes at the 300 -level are offered next semester ?
Which 300 -level classes are offered for next semester ?
Which 300 -level classes will be offered next semester ?
Which 300 -level courses are presented after this semester ?
What courses are only offered in the Winter ?
Are there any courses that are only offered in Spring ?
Are there courses that are only available in the Spring-Summer ?
Of the courses offered , which ones are only available in the Spring ?
What are the courses which are only offered in the Winter ?
What classes are only available in the Spring-Summer ?
What classes can I only take in the Spring ?
What classes can I take only in the Summer ?
Which courses are only available during Spring ?
Which courses can I only take in Winter ?
Which Winter courses are only offered then ?
What classes will I be able to take once I take MUSPERF 990 ?
After GREEK 550 what courses can I take ?
Are there any courses that I can take that RCARTS 268 is a pre-requisite for ?
For which classes is PHARMSCI 761 a prerequisite ?
If I take PHARMACY 426 , what classes will I be able to take ?
Once I take RUSSIAN 201 , what classes will I be able to take ?
What are the additional classes that I can take after finishing GERMAN 171 ?
What classes am I eligible for after completing PAT 102 ?
What courses are available for me to participate in once I finish AUTO 563 ?
What courses open up after taking BUDDHST 512 ?
Which classes will I be able to take once I complete ELI 533 ?
Which courses are available after taking HJCS 277 ?
Which upper-level classes have projects but no exams ?
Are there upper level classes that have projects but no exams ?
Are there upper-level classes that have projects but do not have exams ?
Are there upper-level classes that have projects without exams ?
Do any upper-level classes exist that have projects but no exams ?
Is there any upper-level classes without exams that have projects ?
What classes in the upper-level have projects but no exams .
What upper level classes are offered that do n't require exams but have projects ?
What upper level classes can I take to avoid exams and only do projects ?
What upper-level classes require projects in place of exams .
Which classes are upper level and have projects but not exams ?
Which classes have projects , no exams , and are upper level ?
Which classes in the upper-level have projects and do n't have exams ?
Which upper level classes use projects rather than exams for evaluation ?
Which upper-level classes exempt final exams but require projects ?
Which upper-level classes have no exams but require projects ?
Which upper-level classes require only projects and no exams ?
Do all upper-level classes have exams ?
Are exams always a part of the upper-level classes ?
Are exams necessary for all upper-level classes ?
Are exams part of all upper-level classes ?
Are there exams for all upper-level classes ?
Are there exams in all upper-level classes ?
Do all senior level classes require exams ?
Do all the classes in the upper-level have exams ?
Do all upper-level classes contain exams ?
Do upper-level classes all have exams ?
Does every upper-level class have an examination ?
Does every upper-level class have exams ?
For upper-level classes , do they all have exams ?
Is it true that all upper-level classes have exams ?
Is there an exam for every upper-level class ?
Who teaches Adapting Japanese Fiction to Film this semester ?
Research Health Ed is taught by whom this semester ?
Senior Honors Research I for Psychology as a Social Science will be taught by whom this semester ?
Can you tell me who teaches Great Books of Spain and Latin America this semester ?
Do you know who teaches History of Decor this semester ?
Do you know who the instructor is for Interdisciplinary English this semester ?
For the Business Changing Times class , who is the teacher ?
For this semester 's Intro Med Micro class , who is the instructor ?
This semester , Thermodynamics and Kinetics is taught by whom ?
This semester , who is teaching Sexuality and Science ?
This semester , who is the teacher of Sound Patterns ?
This semester who is teaching Upper-Level Writing ?
This semester who will be the professor for Intrapartum , Postpartum , and Newborn Care ?
This semester who will teach Advanced Al Techniques ?
What professors teach Mass Media and Political Behavior this semester ?
Which professor is teaching Interdisciplinary the Middle Ages this semester ?
Which teacher will teach Lyric , Elegy , and Iambus this semester ?
Who are my options for Teach Elem School professors ?
Who is going to teach Stage Management for this semester ?
Who is teaching Field Geology Project ?
Who is the teacher of Antebellum Society and the Civil War in this semester ?
Who is the teacher this semester for the Cell Cycle Control and Cancer class ?
Who is this semester 's Evolution (UMBS) teacher ?
Who will be teaching Prin Rad Imag this semester ?
I need to leave for work at 5:00 every day , so if I take 499 and 162 , can I do that ?
Are 381 and 328 over by 5:00 each day ?
Are 830 and 683 over by 5:00 every day , because I need to leave for work by then .
As I need to leave for work at 5:00 everyday , can I take 350 and 711 ?
Can I leave at 5:00 P.M. every day if I take 370 and 489 ?
Can I take 611 and 539 as I need to leave for work at 5:00 everyday ?
If I need to leave for work at 5:00 every day , can I take 571 and 775 and still be able to do that ?
If I take 746 and 886 , can I leave at 5 ?
If I take 254 and 125 , can I leave for work at 5:00 every day ?
If I take 525 and 334 , will I be able to leave for work at 5:00 ?
If I take the 358 and 484 , can I leave for work at 5:00 ?
Is it possible to leave for work at 5:00 while taking 866 and 975 ?
Will I be finished with my classes for the day by 5:00 P.M. if I take 431 and 823 ?
Will taking the 615 and 554 allow me to leave for work at 5 each day ?
Can I take 300 -level classes in the Spring or Summer term ?
Am I able to take 100 -level classes in Spring or Summer term ?
Are 300 -level classes offered for me in Spring term or Summer term ?
Are there any 100 -level courses in the Spring or Summer term that I can take ?
Can 500 -level classes be taken by me in the Spring or Summer term ?
Can I enroll in 500 -level classes for the Spring or Summer term ?
During the Spring or Summer term can I take 200 -level classes ?
For the Spring or Summer term , can I take 100 -level classes ?
Is it ok to take 300 -level classes in Spring or Summer term ?
Is it possible for me to take the 100 -level classes , in the Spring or Summer term ?
May I sign up for 100 -level classes in Spring or Summer term ?
Are there any 100 -level classes in Fall or Winter term ?
500 -level classes , are they offered in the Fall or Winter term ?
Any 100 -level classes held in Fall or Winter term ?
Are 400 -level classes available in either the Fall or Winter terms ?
Are 500 -level classes available in Fall or Winter term ?
Are any 300 -level classes being offered in the Fall or Winter term ?
Are there any Fall or Winter classes available for the 200 -level ?
Can 100 -level classes be taken in Fall or Winter ?
Can the 200 -level classes be taken in the Fall or Winter term ?
Do any 400 -level classes occur in the Fall or Winter term ?
Do you know if 300 -level classes are being held in Fall or Winter term ?
Does the Fall or Winter term offer any 200 -level classes ?
During the Fall or Winter terms , are any 500 -level classes being offered ?
For the Fall or Winter term , are there any 100 -level classes ?
In Fall or Winter term are there any 500 -level classes ?
In the Fall or Winter term are there 500 -level classes available to take ?
In the Fall or Winter term are there any 500 -level classes ?
What 100 -level classes do you have available for the Fall or Winter term ?
What 300 -level courses are available in Fall or Winter terms ?
Which 300 -level classes are being taught in Fall and Winter term ?
Which 300 -level classes are there for Fall or Winter terms ?
What classes this semester are a prerequisite to most other classes ?
Which class is a prereq for lots of other classes and offered this semester ?
Which class this semester is a prerequisite for the majority of classes I could take later ?
Which course this semester meets the prerequisites for most other courses ?
Which prerequisite for most classes is offered this semester ?
What class is a prerequisite to most other classes , and available this semester ?
This semester , which classes are prerequisites to most other ones ?
Which courses offered this semester unlock most other classes ?
Which classes are prerequisites for most other classes and available this semester ?
Which classes offered this term will be a prerequisite for most other classes ?
Which classes are prerequisites for most of the other classes and available this semester ?
What is the most common prerequisite this semester ?
How many 300 -level classes are being offered in Fall and Winter term ?
How many 300 -level classes are in the Fall or Winter term ?
During the Fall and Winter term , how many 500 level classes are offered ?
For Fall and the Winter terms , what number of 200 -level courses are available ?
During the Fall and Winter terms , how many 500 -level classes are expected to be offered ?
In the Fall and Winter term how many classes offered are 100 -level classes ?
In the Fall and Winter term , how many 300 -level classes are being offered ?
What amount of 100 -level courses are going to be open this Fall and Winter semester ?
How many classes offered in the Fall Winter term are 300 -level classes ?
How many different classes at level 300 are there to take for the Fall and Winter ?
What amount of 200 -level classes are available for Fall and Winter ?
What number of 100 -level courses are offered in Fall and Winter term ?
Of the classes being offered in Fall and Winter term , how many are 200 -level ?
Fall and Winter terms offer how many 200 -level classes ?
For the Winter and Fall terms , how many 400 -level classes are being offered ?